<?php

use _34ml\SyncSheetDataPage\Http\Controllers\SyncController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

// Route::get('/endpoint', function (Request $request) {
//     //
// });

     Route::Post('/google-sheet-sync',[SyncController::class, 'sync']);
     Route::Post('/google-sheet-sync-history',[SyncController::class, 'history']);
     Route::Post('/google-sheet-sync-job-details',[SyncController::class, 'details']);
