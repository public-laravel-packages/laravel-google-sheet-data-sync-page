<?php

namespace _34ml\SyncSheetDataPage\Http\Controllers;

use _34ML\GoogleSheetsClient\Models\ImportJob;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Throwable;

class SyncController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function sync(Request $request)
    {
        $target_module = config('sync-modules.' . $request->module);
        $from = $request->from;
        $to =  $request->to;
        try {
            $target_module::enqueue($from,$to);
            return json_encode(["status" => "success"]);
        }
        catch (Throwable $exception)
        {
            $user = auth()->user();
            \App\Helpers\SendSyncResultsEmail::sendMail($exception->getMessage(), $user->email);
            return json_encode(["status" => "fail", "message" => $exception->getMessage()]);
        }

    }

    public function history()
    {
        $jobs = ImportJob::orderByDesc('created_at')->get();
        return json_encode(["status" => "success", "data" => $jobs]);
    }

    public function details(Request $request)
    {
        $job = ImportJob::with('rows')->findOrFail($request->id);
        return json_encode(["status" => "success", "data" => $job->rows]);
    }
}
