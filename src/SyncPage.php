<?php

namespace _34ml\SyncSheetDataPage;

use Laravel\Nova\Nova;
use Laravel\Nova\Tool;

class SyncPage extends Tool
{
    /**
     * Perform any tasks that need to happen when the tool is booted.
     *
     * @return void
     */
    public function boot()
    {
        Nova::script('sync-sheet-data-page', __DIR__.'/../dist/js/tool.js');
        Nova::style('sync-sheet-data-page', __DIR__.'/../dist/css/tool.css');
    }

    /**
     * Build the view that renders the navigation links for the tool.
     *
     * @return \Illuminate\View\View
     */
    public function renderNavigation()
    {
        return view('sync-sheet-data-page::navigation');
    }
}
