<?php

namespace _34ml\SyncSheetDataPage;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;
use _34ml\SyncSheetDataPage\Http\Middleware\Authorize;

class SyncSheetDataPageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->config();

        $this->nova();
        $this->app->booted(function () {
            $this->routes();
        });

        Nova::serving(function (ServingNova $event) {
            Nova::provideToScript([
                'modules' => config('sync-modules')
            ]);
        });
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'sync-sheet-data-page');
    }
    /**
     * Bootstrap Nova resources and components.
     */
    protected function nova()
    {
        Nova::serving(
            function () {
                Nova::script('syncer', __DIR__ . '/../dist/js/tool.js');
                Nova::style('syncer', __DIR__ . '/../dist/css/tool.css');
            }
        );
    }
    /**
     * Register the tool's routes.
     *
     * @return void
     */
    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        Route::middleware(['nova', Authorize::class])
                ->prefix('nova-vendor/sync-sheet-data-page')
                ->group(__DIR__.'/../routes/api.php');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function config()
    {
        $this->publishes(
            [
                __DIR__ . '/../config/sync-modules.php' => config_path('sync-modules.php'),
            ]
        );
    }

}
